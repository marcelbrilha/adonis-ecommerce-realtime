'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Category = use('App/Models/Category')
const Transformer = use('App/Transformers/Admin/CategoryTransformer')

class CategoryController {
  /**
   * Show a list of all categories.
   * GET categories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {TransformWith} ctx.transform
   * @param {Object} ctx.pagination
   */
  async index({ request, response, transform, pagination }) {
    const query = Category.query()
    const title = request.input('title')
    const { page, limit } = pagination

    if (title) query.where('title', 'LIKE', `%${title}%`)

    const result = await query.paginate(page, limit)
    const categories = await transform.paginate(result, Transformer)

    return response.ok(categories)
  }

  /**
   * Create/save a new category.
   * POST categories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {TransformWith} ctx.transform
   * @param {Response} ctx.response
   */
  async store({ request, response, transform }) {
    try {
      const { title, description, image_id } = request.all()
      const result = await Category.create({ title, description, image_id })
      const category = await transform.item(result, Transformer)

      return response.created(category)
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao salvar categoria.'
      })
    }
  }

  /**
   * Display a single category.
   * GET categories/:id
   *
   * @param {object} ctx
   * @param {TransformWith} ctx.transform
   * @param {Response} ctx.response
   */
  async show({ params: { id }, response, transform }) {
    const result = await Category.findOrFail(id)
    const category = await transform.item(result, Transformer)

    return response.ok(category)
  }

  /**
   * Update category details.
   * PUT or PATCH categories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {TransformWith} ctx.transform
   * @param {Response} ctx.response
   */
  async update({ params: { id }, request, response, transform }) {
    const result = await Category.findOrFail(id)
    const category = await transform.item(result, Transformer)

    try {
      const { title, description, image_id } = request.all()

      category.merge({ title, description, image_id })
      await category.save()

      return response.ok(category)
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao atualizar categoria.'
      })
    }
  }

  /**
   * Delete a category with id.
   * DELETE categories/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, response }) {
    const category = await Category.findOrFail(id)

    try {
      await category.delete()
      return response.noContent()
    } catch (error) {
      return response.internalServerError({
        message: 'Erro ao remover categoria.'
      })
    }
  }
}

module.exports = CategoryController
