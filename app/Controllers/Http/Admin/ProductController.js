'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Product = use('App/Models/Product')
const Transformer = use('App/Transformers/Admin/ProductTransformer')

class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Object} ctx.pagination
   */
  async index({ request, response, pagination, transform }) {
    const { page, limit } = pagination
    const name = request.input('name')
    const query = Product.query()

    if (name) query.where('name', 'LIKE', `%${name}%`)

    const result = await query.paginate(page, limit)
    const products = await transform.paginate(result, Transformer)
    return response.ok(products)
  }

  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, transform }) {
    try {
      const { name, description, price, image_id } = request.all()
      const result = await Product.create({
        name,
        description,
        price,
        image_id
      })

      const product = await transform.item(result, Transformer)
      return response.created(product)
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao criar o produto.'
      })
    }
  }

  /**
   * Display a single product.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async show({ params: { id }, response, transform }) {
    const result = await Product.findOrFail(id)
    const product = await transform.item(result, Transformer)

    return response.ok(product)
  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params: { id }, request, response }) {
    const product = await Product.findOrFail(id)

    try {
      const { name, description, price, image_id } = request.all()

      product.merge({ name, description, price, image_id })
      await product.save()

      const productTransform = await transform.item(product, Transformer)

      return response.ok(productTransform)
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao atualizar o produto.'
      })
    }
  }

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, response }) {
    const product = await Product.findOrFail(id)

    try {
      await product.delete()
      return response.noContent()
    } catch (error) {
      return response.internalServerError({
        message: 'Erro ao remover o produto.'
      })
    }
  }
}

module.exports = ProductController
