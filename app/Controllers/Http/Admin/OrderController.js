'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Order = use('App/Models/Order')
const Database = use('Database')
const Service = use('App/Services/Order/OrderService')
const Coupon = use('App/Models/Coupon')
const Discount = use('App/Models/Discount')
const Transformer = use('App/Transformers/Admin/OrderTransformer')

class OrderController {
  /**
   * Show a list of all orders.
   * GET orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Object} ctx.pagination
   */
  async index({ request, response, pagination, transform }) {
    const { page, limit } = pagination
    const { status, id } = request.only(['status', 'id'])
    const query = Order.query()

    if (status && id) {
      query.where('status', status)
      query.orWhere('id', 'LIKE', `%${id}%`)
    } else if (status) {
      query.where('status', status)
    } else if (id) {
      query.where('id', 'LIKE', `%${id}%`)
    }

    const result = await query.paginate(page, limit)
    const orders = await transform.paginate(result, Transformer)
    return response.ok(orders)
  }

  /**
   * Create/save a new order.
   * POST orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const transaction = await Database.beginTransaction()

    try {
      const { user_id, items, status } = request.all()
      const order = await Order.create({ user_id, items, status }, transaction)
      const service = new Service(order, transaction)

      if (items && items.length > 0) await service.syncItems(items)

      await transaction.commit()

      return response.created(order)
    } catch (error) {
      await transaction.rollback()
      return response.badRequest({
        message: 'Erro ao criar pedido.'
      })
    }
  }

  /**
   * Display a single order.
   * GET orders/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async show({ params: { id }, response }) {
    const order = await Order.findOrFail(id)
    return response.ok(order)
  }

  /**
   * Update order details.
   * PUT or PATCH orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params: { id }, request, response }) {
    const order = await Order.findOrFail(id)
    const transaction = await Database.beginTransaction()

    try {
      const { user_id, items, status } = request.all()
      const service = new Service(order, transaction)

      order.merge({ user_id, status })

      await service.updateItems(items)
      await order.save(transaction)
      await transaction.commit()

      return response.ok(order)
    } catch (error) {
      await transaction.rollback()
      return response.badRequest({
        message: 'Erro ao atualizar pedido.'
      })
    }
  }

  /**
   * Delete a order with id.
   * DELETE orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, request, response }) {
    const order = await Order.findOrFail(id)
    const transaction = Database.beginTransaction()

    try {
      await order.items().delete(transaction)
      await order.coupons().delete(transaction)
      await order.discounts().delete(transaction)
      await order.delete(transaction)
      await transaction.commit()

      return response.noContent()
    } catch (error) {
      transaction.rollback()
      return response.internalServerError({
        message: 'Erro ao remover pedido.'
      })
    }
  }

  /**
   * Apply discount
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async applyDiscount({ params: { id }, request, response }) {
    const { code } = request.all()
    const coupon = await Coupon.findByOrFail('code', code.toUpperCase())
    const order = await Order.findOrFail(id)
    let discount
    let info = {}

    try {
      const service = new Service(order)
      const canAddDiscount = await service.canApplyDiscount(coupon)
      const orderDiscounts = await order.coupons().getCount()
      const canApplyToOrder =
        orderDiscounts < 1 || (orderDiscounts >= 1 && coupon.recursive)

      if (canAddDiscount && canApplyToOrder) {
        discount = await Discount.findOrCreate({
          order_id: order.id,
          coupon_id: coupon.id
        })

        info.message = 'Cupon aplicado com sucesso'
        info.success = true
      } else {
        info.message = 'Não foi possível aplicar esse cupom'
        info.success = false
      }

      return response.send({ order, info })
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao aplicar o cupom.'
      })
    }
  }

  async removeDiscount({ request, response }) {
    const { discount_id } = request.all()
    const discount = await Discount.findOrFail(discount_id)

    await discount.delete()

    return response.noContent()
  }
}

module.exports = OrderController
