'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Coupon = use('App/Models/Coupon')
const CouponService = use('App/Services/Coupon/CouponService')
const Database = use('Database')
const { getCanUseFor } = use('App/Helpers')

class CouponController {
  /**
   * Show a list of all coupons.
   * GET coupons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Object} ctx.pagination
   */
  async index({ request, response, pagination }) {
    const { page, limit } = pagination
    const code = request.input('code')
    const query = Coupon.query()

    if (code) query.where('code', 'LIKE', `%${code}%`)

    const coupons = await query.paginate(page, limit)
    return response.ok(coupons)
  }

  /**
   * Create/save a new coupon.
   * POST coupons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const canUseFor = { client: false, product: false }

    try {
      const transaction = Database.beginTransaction()
      const { users, products } = request.only(['users', 'products'])
      const couponData = request.only([
        'code',
        'discount',
        'valid_from',
        'valid_until',
        'quantity',
        'type',
        'recursive'
      ])

      const coupon = await Coupon.create(couponData, transaction)
      const service = new CouponService(coupon, transaction)

      if (users && users.length > 0) {
        await service.syncUsers(users)
        canUseFor.client = true
      }

      if (products && products.length > 0) {
        await service.syncProducts(products)
        canUseFor.product = true
      }

      coupon.can_use_for = getCanUseFor(canUseFor)

      await coupon.save(transaction)
      await transaction.commit()

      return response.created(coupon)
    } catch (error) {
      await transaction.rollback()
      return response.badRequest({
        message: 'Erro ao salvar cupom.'
      })
    }
  }

  /**
   * Display a single coupon.
   * GET coupons/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async show({ params: { id }, response }) {
    const coupon = await Coupon.findOrFail(id)
    return response.ok(coupon)
  }

  /**
   * Update coupon details.
   * PUT or PATCH coupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params: { id }, request, response }) {
    const canUseFor = { client: false, product: false }
    const transaction = Database.beginTransaction()
    const coupon = await Coupon.findOrFail(id)

    try {
      const couponData = request.only([
        'code',
        'discount',
        'valid_from',
        'valid_until',
        'quantity',
        'type',
        'recursive'
      ])

      coupon.merge(couponData)

      const { users, products } = request.only(['users', 'products'])
      const service = new CouponService(coupon, transaction)

      if (users && users.length > 0) {
        await service.syncUsers(users)
        canUseFor.client = true
      }

      if (products && products.length > 0) {
        await service.syncProducts(products)
        canUseFor.product = true
      }

      coupon.can_use_for = getCanUseFor(canUseFor)

      await coupon.save()
      await transaction.commit()

      return response.ok(coupon)
    } catch (error) {
      await transaction.rollback()
      return response.badRequest({
        message: 'Erro ao atualizar o cupom.'
      })
    }
  }

  /**
   * Delete a coupon with id.
   * DELETE coupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, request, response }) {
    const transaction = Database.beginTransaction()
    const coupon = await Coupon.findOrFail(id)

    try {
      await coupon.products().detach([], transaction)
      await coupon.orders().detach([], transaction)
      await coupon.users().detach([], transaction)
      await coupon.delete(transaction)
      await transaction.commit()

      return response.noContent()
    } catch (error) {
      await transaction.rollback()
      return response.internalServerError({
        message: 'Erro ao remover cupom.'
      })
    }
  }
}

module.exports = CouponController
