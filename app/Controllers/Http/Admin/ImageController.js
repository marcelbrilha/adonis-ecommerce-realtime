'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Image = use('App/Models/Image')
const { manageSingleUpload, manageMultipleUpload } = use('App/Helpers')
const fs = use('fs')
const Transformer = use('App/Transformers/Admin/ImageTransformer')
const Helpers = use('Helpers')

class ImageController {
  /**
   * Show a list of all images.
   * GET images
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Object} ctx.pagination
   */
  async index({ response, pagination, transform }) {
    const { page, limit } = pagination
    const result = await Image.query()
      .orderBy('id', 'DESC')
      .paginate(page, limit)

    const images = await transform.paginate(result, Transformer)

    return response.ok(images)
  }

  /**
   * Create/save a new image.
   * POST images
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, transform }) {
    try {
      const images = []
      const fileJar = request.file('images', { types: ['image'], size: '2mb' })

      if (!fileJar.files) {
        const file = await manageSingleUpload(fileJar)

        if (file.moved()) {
          const image = await Image.create({
            path: file.fileName,
            size: file.size,
            original_name: file.clientName,
            extension: file.subtype
          })

          const transformedImage = await transform.item(image, Transformer)

          images.push(transformedImage)

          return response.created({ success: images, errors: {} })
        }

        return response.badRequest({
          message: 'Não foi possível processar essa imagem no momento.'
        })
      }

      const files = await manageMultipleUpload(fileJar)

      await Promise.all(
        files.success.map(async file => {
          const image = await Image.create({
            path: file.fileName,
            size: file.size,
            original_name: file.clientName,
            extension: file.subtype
          })

          const transformedImage = await transform.item(image, Transformer)
          images.push(transformedImage)
        })
      )

      return response.created({ success: images, errors: files.errors })
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao processar a sua solicitação.'
      })
    }
  }

  /**
   * Display a single image.
   * GET images/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async show({ params: { id }, response, transform }) {
    const result = await Image.findOrFail(id)
    const image = transform.item(result, Transformer)
    return response.ok(image)
  }

  /**
   * Update image details.
   * PUT or PATCH images/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params: { id }, request, response, transform }) {
    const result = await Image.findOrFail(id)
    const image = transform.item(result, Transformer)

    try {
      image.merge(request.only(['original_name']))
      await image.save()

      return response.ok({ data: image })
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao atualizar imagem.'
      })
    }
  }

  /**
   * Delete a image with id.
   * DELETE images/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, request, response }) {
    const image = await Image.findOrFail(id)

    try {
      const filePath = Helpers.publicPath(`uploads/${image.path}`)

      fs.unlinkSync(filePath)
      await image.delete()

      return response.noContent()
    } catch (error) {
      return response.internalServerError({
        message: 'Erro ao remover imagem.'
      })
    }
  }
}

module.exports = ImageController
