'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const User = use('App/Models/User')
const Transformer = use('App/Transformers/Admin/UserTransformer')

class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Object} ctx.pagination
   */
  async index({ request, response, pagination, transform }) {
    const { page, limit } = pagination
    const name = request.input('name')
    const query = User.query()

    if (name) {
      query.where('name', 'LIKE', `%${name}%`)
      query.orWhere('surname', 'LIKE', `%${name}%`)
      query.orWhere('email', 'LIKE', `%${name}%`)
    }

    const result = await query.paginate(page, limit)
    const users = await transform.paginate(result, Transformer)
    return response.ok(users)
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, transform }) {
    try {
      const { name, surname, email, password, image_id } = request.all()
      const result = await User.create({
        name,
        surname,
        email,
        password,
        image_id
      })

      const user = await transform.item(result, Transformer)
      return response.created(user)
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao criar usuário.'
      })
    }
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async show({ params: { id }, response, transform }) {
    const result = await User.findOrFail(id)
    const user = await transform.item(result, Transformer)
    return response.ok(user)
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params: { id }, request, response }) {
    const result = await User.findOrFail(id)

    try {
      const { name, surname, email, password, image_id } = request.all()

      result.merge({ name, surname, email, password, image_id })
      await result.save()

      const user = await transform.item(result, Transformer)
      return response.ok(user)
    } catch (error) {
      return response.badRequest({
        message: 'Erro ao atualizar usuário.'
      })
    }
  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, response }) {
    const user = await User.findOrFail(id)

    try {
      await user.delete()
      return response.noContent()
    } catch (error) {
      return response.internalServerError({
        message: 'Erro ao remover usuário.'
      })
    }
  }
}

module.exports = UserController
