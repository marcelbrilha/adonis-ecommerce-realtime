'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Order = use('App/Models/Order')
const Transformer = use('App/Transformers/Admin/OrderTransformer')
const Database = use('Database')
const Service = use('App/Services/Order/OrderService')
const Coupon = use('App/Models/Coupon')
const Discount = use('App/Models/Discount')
const Ws = use('Ws')

/**
 * Resourceful controller for interacting with orders
 */
class OrderController {
  /**
   * Show a list of all orders.
   * GET orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response, transform, pagination, auth }) {
    const client = await auth.getUser()
    const number = request.input('number')
    const query = Order.query()

    if (number) query.where('id', 'LIKE', `${number}`)
    query.where('user_id', client.id)

    const results = await query
      .orderBy('id', 'DESC')
      .paginate(pagination.page, pagination.limit)

    const order = await transform.paginate(results, Transformer)

    return response.send(orders)
  }

  /**
   * Create/save a new order.
   * POST orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth, transform }) {
    const transaction = await Database.beginTransaction()

    try {
      const items = request.input('items')
      const client = await auth.getUser()
      let order = await Order.create({ user_id: client.id }, transaction)

      const service = new Service(order, transaction)

      if (items.length > 0) {
        await service.syncItems(items)
      }

      await transaction.commit()

      order = await Order.find(order.id)
      order = await transform.include('items').item(order, Transformer)

      const topic = Ws.getChannel('notifications').topic('notifications')

      if (topic) topic.broadcast('new:order', order)

      return response.status(201).send(order)
    } catch (error) {
      await transaction.rollback()

      return response
        .status(400)
        .send({ message: 'Não foi possível fazer o seu pedido' })
    }
  }

  /**
   * Display a single order.
   * GET orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params: { id }, request, response, transform, auth }) {
    const client = await auth.getUser()
    const result = await Order.query()
      .where('user_id', client.id)
      .where('id', id)
      .firstOrFail()
    const order = await transform.item(result, Transformer)

    return response.send(order)
  }

  /**
   * Update order details.
   * PUT or PATCH orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response, auth, transform }) {
    const client = await auth.getUser()
    let order = await Order.query()
      .where('user_id', client.id)
      .where('id', id)
      .firstOrFail()

    const transaction = await Database.beginTransaction()

    try {
      const { items, status } = request.all()
      order.merge({ user_id: client.id, status })
      const service = new Service(order, transaction)

      await service.updateItems(items)
      await order.save(transaction)
      await transaction.commit()

      order = await transform
        .include('items,coupons,discounts')
        .item(order, Transformer)

      return response.send(order)
    } catch (error) {
      await transaction.rollback()
      return response
        .status(400)
        .send({ message: 'Não foi possível atualizar o seu pedido' })
    }
  }

  async applyDiscount({ params: { id }, request, response, transform, auth }) {
    const { code } = request.all()
    const client = await auth.getUser()
    const coupon = await Coupon.findByOrFail('code', code.toUpperCase())
    let order = await Order.query()
      .where('user_id', client.id)
      .where('id', id)
      .firstOrFail()

    let discount,
      info = {}

    try {
      const service = new Service(order)
      const canAddDiscount = await service.canApplyDiscount(coupon)
      const orderDiscounts = await order.coupons().getCount()

      const canApplyToOrder =
        orderDiscounts < 1 || (orderDiscounts >= 1 && coupon.recursive)

      if (canAddDiscount && canApplyToOrder) {
        discount = await Discount.findOrCreate({
          order_id: order.id,
          coupon_id: coupon.id
        })

        info.message = 'Cupon aplicado com sucesso'
        info.success = true
      } else {
        info.message = 'Não foi possível aplicar o cupom'
        info.success = false
      }

      order = await transform
        .include('coupons,items,discounts')
        .item(order, Transformer)

      return response.send({ order, info })
    } catch (error) {
      return response.status(400).send({
        message: 'Erro desconhecido'
      })
    }
  }

  async removeDiscount({ params: { id }, request, response, auth }) {
    const { discount_id } = request.all()
    const discount = await Discount.findByOrFail(discount_id)
    await discount.delete()
    return response.status(204).send()
  }
}

module.exports = OrderController
