'use strict'

const Database = use('Database')
const User = use('App/Models/User')
const Role = use('Role')
const Ws = use('Ws')

class AuthController {
  _getRefreshToken(request) {
    let refreshToken = request.input('refresh_token')
    if (!refreshToken) refreshToken = request.header('refresh_token')
    return refreshToken
  }

  async register({ request, response }) {
    const transaction = await Database.beginTransaction()

    try {
      const userRole = await Role.findBy('slug', 'client')
      const { name, surname, email, password } = request.all()
      const user = await User.create(
        { name, surname, email, password },
        transaction
      )

      await user.roles().attach([userRole.id], null, transaction)
      await transaction.commit()

      const topic = Ws.getChannel('notifications').topic('notifications')

      if (topic) topic.broadcast('new:user')

      return response.created(user)
    } catch (error) {
      await transaction.rollback()
      return response.badRequest({
        message: 'Erro ao realizar cadastro.'
      })
    }
  }

  async login({ request, response, auth }) {
    const { email, password } = request.all()
    const data = await auth.withRefreshToken().attempt(email, password)

    return response.ok(data)
  }

  async refresh({ request, response, auth }) {
    const refreshToken = _getRefreshToken(request)
    const user = await auth
      .newRefreshToken()
      .generateForRefreshToken(refreshToken)

    return response.ok(user)
  }

  async logout({ request, response, auth }) {
    const refreshToken = _getRefreshToken(request)
    await auth.authenticator('jwt').revokeTokens([refreshToken], true)

    return response.noContent()
  }

  async forgot({ request, response }) {}

  async remember({ request, response }) {}

  async reset({ request, response }) {}
}

module.exports = AuthController
