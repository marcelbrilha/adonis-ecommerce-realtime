'use strict'

const crypto = use('crypto')
const Helpers = use('Helpers')

/**
 * Generate random string
 *
 * @param {Int} length
 * @return {string} Random string
 */
const strRandom = async (length = 40) => {
  let string = ''

  if (string.length < length) {
    const size = length - string.length
    const bytes = await crypto.randomBytes(size)
    const buffer = Buffer.from(bytes)

    string += buffer
      .toString('base64')
      .replace(/[^a-zA-Z0-9]/g, '')
      .substr(0, size)
  }

  return string
}

/**
 * Move file for path
 *
 * @param {FileJar} file
 * @param {string} path
 * @return {object<FileJar>}
 */
const manageSingleUpload = async (file, path = null) => {
  const internalPath = path ? path : Helpers.publicPath('uploads')
  const randomName = await strRandom(30)
  const filename = `${new Date().getTime()}-${randomName}.${file.subtype}`

  await file.move(internalPath, { name: filename })

  return file
}

/**
 * Move files for path
 *
 * @param {FileJar} fileJar
 * @param {string} path
 * @return {object}
 */
const manageMultipleUpload = async (fileJar, path = null) => {
  const internalPath = path ? path : Helpers.publicPath('uploads')
  const success = []
  const errors = []

  await Promise.all(
    fileJar.files.map(async file => {
      const randomName = await strRandom(30)
      const filename = `${new Date().getTime()}-${randomName}.${file.subtype}`

      await file.move(internalPath, { name: filename })

      if (file.moved()) {
        success.push(file)
      } else {
        errors.push(file.error())
      }
    })
  )

  return { success, errors }
}

/**
 * Get Can use for - Coupon
 *
 * @param {object} canUseFor
 */
const getCanUseFor = canUseFor => {
  let canUseForCoupon = 'all'
  const canUseForCouponRules = {
    product_client: canUseFor.product && canUseFor.client,
    product: canUseFor.product && !canUseFor.client,
    client: !canUseFor.product && canUseFor.client
  }

  Object.keys(canUseForCouponRules).forEach(key => {
    if (canUseForCouponRules[key]) canUseForCoupon = key
  })

  return canUseForCoupon
}

module.exports = {
  strRandom,
  manageSingleUpload,
  manageMultipleUpload,
  getCanUseFor
}
