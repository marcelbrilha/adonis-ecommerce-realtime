'use strict'

class Login {
  get rules() {
    return {
      email: 'required|email',
      password: 'required'
    }
  }

  get messages() {
    return {
      'email.required': 'Campo e-mail é obrigatório',
      'email.email': 'E-mail inválido',
      'password.required': 'Campo senha é obrigatório'
    }
  }
}

module.exports = Login
