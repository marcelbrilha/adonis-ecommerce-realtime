'use strict'

const Route = use('Route')

require('./auth')
require('./admin')
require('./client')

Route.get('v1/me', 'UserController.me')
  .as('me')
  .middleware('auth')
